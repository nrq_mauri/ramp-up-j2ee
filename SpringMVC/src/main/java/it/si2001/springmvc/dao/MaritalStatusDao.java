package it.si2001.springmvc.dao;

import it.si2001.springmvc.models.MaritalStatus;
import org.springframework.stereotype.Repository;

/**
 * Created by mauricio on 8/21/17.
 * NrqApps © 2017
 */
@Repository("maritalStatusDao")
public class MaritalStatusDao extends AbstractDao<Integer, MaritalStatus> {

}
