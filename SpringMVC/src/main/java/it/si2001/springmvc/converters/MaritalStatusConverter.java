package it.si2001.springmvc.converters;

import it.si2001.springmvc.models.MaritalStatus;
import it.si2001.springmvc.service.MaritalStatusService;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by mauricio on 8/21/17.
 * NrqApps © 2017
 */
public class MaritalStatusConverter implements Converter<String, MaritalStatus> {

    private MaritalStatusService maritalStatusService;

    public MaritalStatusConverter(MaritalStatusService maritalStatusService){
        this.maritalStatusService = maritalStatusService;
    }

    @Override
    public MaritalStatus convert(String s) {
        try {
            Integer maritalStatusId = Integer.parseInt(s);
            return maritalStatusService.findOne(maritalStatusId);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
