package it.si2001.springmvc.dao;

import it.si2001.springmvc.models.Skill;
import org.springframework.stereotype.Repository;

/**
 * Created by mauricio on 8/21/17.
 * NrqApps © 2017
 */
@Repository("skillDao")
public class SkillDao extends AbstractDao<Integer, Skill> {

}
