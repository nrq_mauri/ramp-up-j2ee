<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Upsert Employee</title>
    <link type="text/css" rel="stylesheet" href="<c:url value="/static/css/main.css"/>">
    <script type="text/javascript">
      window.initialSkills = ${employeeSkills};
    </script>
</head>
    <body>
    <div class="upsert container">
        <form:form id="upsert-form" modelAttribute="employee" method="post">
            <fieldset class="upsert__fieldset">
                <legend class="upsert__legend">${not empty employee.employeeId ? 'Edit ' : 'Insert '} Employee</legend>
                <label class="upsert__label">
                    <span class="upsert__label-text">Name:</span>
                    <form:input cssClass="upsert__input" path="name" type="text" placeholder="Name" />
                </label>
                <label class="upsert__label">
                    <span class="upsert__label-text">Surname:</span>
                    <form:input cssClass="upsert__input" path="surname" type="text" placeholder="Surname" />
                </label>
                <label class="upsert__label">
                    <span class="upsert__label-text">Country:</span>
                    <form:input cssClass="upsert__input" path="country" type="text" placeholder="Country" />
                </label>
                <label class="upsert__label">
                    <span class="upsert__label-text">Birth Date:</span>
                    <form:input cssClass="upsert__input" path="birthDate" type="date" placeholder="Birth Date" />
                </label>
                <label class="upsert__label">
                    <span class="upsert__label-text">Marital Status:</span>
                    <form:select cssClass="upsert__input" path="maritalStatus" items="${maritalStatusList}" itemLabel="status" itemValue="maritalStatusId" />
                </label>

                <fieldset class="upsert__skills">
                    <legend>Skills</legend>
                    <label>
                        <span>Skills to add:</span>
                        <select id="skills-to-add">
                            <c:forEach var="skill" items="${skills}">
                                <option value="${skill.skillId}-${skill.name}">${skill.name}</option>
                            </c:forEach>
                        </select>
                    </label>
                    <button type="button" class="link-as-button" onclick="addSkill()">Add</button>
                    <div id="skills-container" class="upsert__skills-container"></div>
                    <form:input path="skills" type="hidden" />
                </fieldset>

                <div class="upsert__buttons-container">
                    <input onclick="submitForm()" class="link-as-button" type="button" value="Save">
                    <a href="<c:url value="/"/>" class="link-as-button">Cancel</a>
                </div>
            </fieldset>
        </form:form>
    </div>
    <script src="<c:url value="/static/js/upsert.js"/>" type="text/javascript"></script>
    </body>
</html>
