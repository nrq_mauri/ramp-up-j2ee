package it.si2001.servlets.utils;

import it.si2001.servlets.models.Employee;
import it.si2001.servlets.models.MaritalStatus;
import it.si2001.servlets.models.Skill;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Created by mauricio on 8/17/17.
 * NrqApps © 2017
 */
public class HibernateUtils {
    private static SessionFactory sessionFactory;

    private static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            Configuration configuration = new Configuration().configure("it/si2001/servlets/config/hibernate.cfg.xml");
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(
                    configuration.getProperties()
                ).build();

            configuration
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(MaritalStatus.class)
                .addAnnotatedClass(Skill.class);
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        }

        return sessionFactory;
    }

    public static Session getSession() {
        return getSessionFactory().openSession();
    }
}
